package com.javagda24.springstudent.service;

import com.javagda24.springstudent.model.Grade;
import com.javagda24.springstudent.model.Student;
import com.javagda24.springstudent.repository.GradeRepository;
import com.javagda24.springstudent.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class GradeService {
    @Autowired
    private GradeRepository gradeRepository;
    @Autowired
    private StudentRepository studentRepository;

    public List<Grade> getAll() {
        return gradeRepository.findAll();
    }

    public void save(Grade grade, Long retrievedStudentId) {
        if (studentRepository.existsById(retrievedStudentId)) {
            Student student = studentRepository.getOne(retrievedStudentId);
            grade.setStudent(student);

            gradeRepository.save(grade);
        } else {
            throw new EntityNotFoundException("Student not found.");
        }
    }

    public void deleteById(Long id) {
        gradeRepository.deleteById(id);
    }

    public Optional<Grade> getById(Long id) {
        return gradeRepository.findById(id);
    }
}
