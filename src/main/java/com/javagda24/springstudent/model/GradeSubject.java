package com.javagda24.springstudent.model;

public enum GradeSubject {
    ENGLISH,
    POLISH,
    MATH,
    IT,
    CHEMISTRY,
    PHYSICS,
    HISTORY,
    PE;
}