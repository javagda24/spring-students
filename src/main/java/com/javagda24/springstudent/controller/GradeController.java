package com.javagda24.springstudent.controller;

import com.javagda24.springstudent.model.Grade;
import com.javagda24.springstudent.model.GradeSubject;
import com.javagda24.springstudent.service.GradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

@Controller
@RequestMapping(path = "/grade/")
public class GradeController {
    @Autowired
    private GradeService gradeService;

    @GetMapping("/list")
    public String listGrades(Model model) {
        model.addAttribute("grades", gradeService.getAll());

        return "grade-list";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable(name = "id") Long id) {
        gradeService.deleteById(id);

        return "redirect:/grade/list";
    }

    @GetMapping("/edit/{id}")
    public String edit(Model model,
                       @PathVariable(name = "id") Long id) {
        Optional<Grade> gradeOptional = gradeService.getById(id);
        if (gradeOptional.isPresent()) {
            Grade grade = gradeOptional.get();

            model.addAttribute("gradeToEdit", grade);
            model.addAttribute("subjects", GradeSubject.values());
            model.addAttribute("studentToWhomIAddGrade", grade.getStudent().getId());

            return "grade-form";
        }

        return "redirect:/grade/list";
    }

    @GetMapping("/add/{studentId}")
    public String gradeForm(Model model,
                            @PathVariable(name = "studentId") Long studentId) {
        model.addAttribute("gradeToEdit", new Grade());
        model.addAttribute("subjects", GradeSubject.values());

        // parametr - identyfikator użytkownika któremu dodajemy ocene jest przyjmowany
        //          jako PathVariable, a następnie przekazywany do formularza oceny
        model.addAttribute("studentToWhomIAddGrade", studentId);

        return "grade-form";
    }

    @PostMapping("/add")
    public String save(Grade grade, Long retrievedStudentId) {
        // retrievedStudentId - wartość dociera do metody post z formularza. Musi mieć taką samą nazwę jak
        // ustawione th:name w formularzu.
        gradeService.save(grade, retrievedStudentId);

        return "redirect:/grade/list";
    }
}

