package com.javagda24.springstudent.controller;

import com.javagda24.springstudent.model.Student;
import com.javagda24.springstudent.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(path = "/student/")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @GetMapping("/delete/{student_to_remove}")
    public String usunStudenta(@PathVariable(name = "student_to_remove") Long id) {
        studentService.deleteById(id);

        return "redirect:/student/list";
    }

    @GetMapping("/edit/{edited_student_id}")
    public String pobierzFormularzEdycji(Model model,
                                         @PathVariable(name = "edited_student_id") Long id) {
        Optional<Student> studentOptional = studentService.findById(id); // wyszukaj w bazie rekordu studenta
        if (studentOptional.isPresent()) { // jeśli uda się go odnaleźć
            Student student = studentOptional.get(); // wydobywamy go z optional

            model.addAttribute("student_do_uzupelnienia", student); // umieszczamy jako atrybut (do edycji)
            return "student-form"; // ładujemy formularz
        } else {
            return "redirect:/student/list";// jeśli nie uda się go odnaleźć, to wróć na widok listy.
        }
    }


    @GetMapping("/add")
    public String zaladujFormularz(Model model) {
        model.addAttribute("student_do_uzupelnienia", new Student());
        return "student-form";
    }

    @PostMapping("/add")
    public String zapisz(Student student) {
        studentService.save(student);

        return "redirect:/student/list";
    }


    //    @RequestMapping(path = "/list", method = RequestMethod.GET)
    @GetMapping("/list")
    public String wyswietlListeStudentow(Model model) {
        // model to obiekt który pozwala przekazywać parametry do widoku
        // model posiada mapę atrybutów
        List<Student> listaStudentow = studentService.getAll();

        model.addAttribute("nasza_lista_studentow", listaStudentow);
        /*umieszczamy listę studentów z bazy danych*/

        return "student-list"; /*nazwa pliku html*/
    }

    @GetMapping("/grades/{studentid}")
    public String getStudentGrades(Model model,
                                   @PathVariable(name = "studentid") Long studentId) {
        Optional<Student> studentOptional = studentService.findById(studentId);
        if (studentOptional.isPresent()) {
            Student student = studentOptional.get();

            model.addAttribute("grades", student.getGrades());
            return "grade-list";
        }
        return "redirect:/student/list";
    }
}
